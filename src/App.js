import React, { useState } from 'react';
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";

/* Router Component */
import Home from './pages/Home';
import ImageModal from './pages/ImageModal';

/* Custom CSS */
import './App.css';

function App() {

  const [modal, setModalData] = useState({ display: false, data: {} });

  const showModal = (val) => {
    setModalData({ display: true, data: val });
  }

  const hideModal = () => {
    setModalData({ display: false, data: {} });
  }

  return (
    <Router>
      <Route path="/">
        <Home showModal={showModal} />
      </Route>
      <Route path="/popup">
        <ImageModal modalData={modal} hideModal={hideModal} />
      </Route>
    </Router>
  );
}

export default App;
