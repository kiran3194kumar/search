import React from 'react';

/* Service */
import {searches} from "../../services/index";

/* Custom CSS */
import './index.css';

function Footer(props) {
    return (
        <div className="footer">
                <h1 className="footer-header">
                    Search<span>it</span>
                </h1>
                <div className="popular-search-container">
                    <h5 className="mb-10">Popular Searches</h5>
                    <ul>
                        {
                            searches.map((val,i)=>(
                                <li key={i} onClick={()=> props.handleSearchTags(val)}>{val}</li>
                            ))
                        }
                    </ul>
                </div>
            </div>
    )
}

export default Footer;