import React, { useEffect, useState } from 'react';
import {
    withRouter
  } from 'react-router-dom'

/* Custom Components */
import Footer from '../Footer/';
import Header from "../Header/";

/* Services */
import { getPhotos, getRandomPhoto, searchPhotos } from "../../services/index";

/* Custom CSS */
import './index.css';

function Home(props) {

    const [images, setImages] = useState([]);
    const [bgImage, setBgImage] = useState('');
    const [searchVal, setSearchVal] = useState('');
    const [pageNo, setPageNo] = useState(1);

    useEffect(() => {
        async function getData() {
            const photos = await getPhotos();
            setImages(photos);
            const res = await getRandomPhoto();
            setBgImage(res.urls.full);
        }
        getData();
    }, [])

    const style = {
        backgroundImage: `url(${bgImage})`
    };

    const handleTextChange = (e) => {
        if (e.key === 'Enter') {
            setSearchImages(e.target.value);
        }
    }

    const setSearchImages = async (searchText) => {
        setSearchVal(searchText);
        setPageNo(1);
        const res = await searchPhotos(1, searchText);
        setImages(res.results)
    }

    const loadMoreData = async () => {
        const newPageNo = pageNo + 1;
        if (searchVal) {
            const res = await searchPhotos(newPageNo, searchVal);
            setImages([...images, ...res.results]);
        } else {
            const res = await getPhotos(newPageNo);
            setImages([...images, ...res]);
        }
        setPageNo(newPageNo);
    }

    const showModal = (val) => {
        props.showModal(val);
        props.history.push('/popup');
    }

    const handleSearchTags = (val) => {
        setSearchImages(val);
    }

    return (
        <div>
            <div className="main-container" style={style} />
            <div className="container">
                <Header handleSearchTags={handleSearchTags} />
                <input className="search-bar" type="text" onKeyDown={handleTextChange} placeholder="Search for images here..." />
                <div className="images">
                    {
                        images.map((val, index) => {
                            return (
                                <div key={`${val.id}-${index}`} onClick={() => showModal(val)}>
                                    <img className="thumb" src={val.urls.thumb} />
                                    <span className="profile-container">
                                        <img className="profile" src={val.user.profile_image.small} />
                                        <span className="profile-text">image by <span>{val.user.name}</span></span>
                                    </span>
                                </div>
                            )
                        })
                    }
                </div>
                <div className="text-center btn-wrapper">
                    <button className="btn-load" onClick={loadMoreData}>
                        Load more
                    </button>
                </div>
            </div>
            <Footer handleSearchTags={handleSearchTags} />
        </div>
    )
}

export default withRouter(Home);