import React from 'react';

/* Custom CSS */
import './index.css';

const ImageModal = React.memo((props) =>  {
 
    const data = Object.keys(props.modalData.data).length;

    return (
        <div className={`modal modal-${props.modalData.display}`}>
              <div className="modal-content">
                    <span className="close" onClick={props.hideModal}>&times;</span>
                    <div className="modal-profile">
                        <img src={data && props.modalData.data.user.profile_image.small}/>
                        <div className="modal-profile-text">
                            <div>{data && props.modalData.data.user.name}</div>
                            <div>{data && props.modalData.data.user.username}</div>
                        </div>
                    </div>
                    <img className="img-large" src={data && props.modalData.data.urls.full}/>
                    <div className="text-center btn-wrapper">
                        <a href={data && props.modalData.data.links.download} target="_blank">
                            <button className="btn-load">Download</button>
                        </a>
                    </div>
              </div>
        </div>
    )
});

export default ImageModal;