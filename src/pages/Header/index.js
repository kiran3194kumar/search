import React from 'react';

/* Services */
import { tags } from "../../services/index";

/* Custom CSS */
import './index.css';

function Header(props) {
    return (
        <>
            <h1 className="header">Search<span>it</span></h1>
            <div className="h-40" />
            <h2>Free Stock photos for everything</h2>
            <h5>We offer the best free stock photo's all in one place</h5>
            <div className="search-tag-container">
                <h5 className="mr-10">Search by tags: </h5>
                <ul>
                    {
                        tags.map((val,i)=>(<li key={i} onClick={()=> props.handleSearchTags(val)}>{val}</li>))
                    }
                </ul>
            </div>
        </>
    )
}

export default Header;