import axios from 'axios';

const AccessKey = "fujtxN2VoTFPay1-3oOqEdrzjJvpZsrGGJoE196kq8U";

const instance = axios.create({
    baseURL: 'https://api.unsplash.com',
    headers: {
        Authorization: `Client-ID ${AccessKey}`,
    }
});

const searches = ["Dog", "Cat", "space", "Nature", "Business", "Office", "Coffee", "World", "Wildlife", "Beach trip", "Digital", "Meeting", "Cars", "Games", "Holiday"]
const tags = ["Dog", "Cat", "space", "Nature", "Business", "Office", "Coffee", "World"];

export function getPhotos(page = 1) {
    return instance.get(`/photos?page=${page}&per_page=9`).then((res) => res.data);
}

export function searchPhotos(page, query) {
    return instance.get(`/search/photos?query=${query}&page=${page}&per_page=9`).then((res) => res.data);
}

export function getRandomPhoto() {
    return instance.get('/photos/random').then((res) => res.data);
}

export { searches, tags };